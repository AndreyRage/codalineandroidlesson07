package me.codaline.lesson07;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.util.concurrent.TimeUnit;

/**
 * Created by rage on 21.03.16.
 */
public class MyLoader extends AsyncTaskLoader<User> {
    private static final String TAG = MyLoader.class.getSimpleName();

    private String param;
    private User mUser;

    public MyLoader(Context context, Bundle args) {
        super(context);
        this.param = args.getString("key");
        Log.d(TAG, "new MyLoader");
    }

    @Override
    public User loadInBackground() {
        Log.d(TAG, "loadInBackground");
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            Log.e(TAG, "error", e);
        }
        mUser = new User(param);
        return mUser;
    }

    @Override
    protected void onStartLoading() {
        Log.d(TAG, "onStartLoading");
        super.onStartLoading();
        if (mUser != null) {
            deliverResult(mUser);
        } else {
            forceLoad();
        }
    }
}
