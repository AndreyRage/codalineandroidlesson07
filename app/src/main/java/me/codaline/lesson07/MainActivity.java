package me.codaline.lesson07;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<User> {
    private static final String TAG = MainActivity.class.getSimpleName();

    private User mUser;

    @Nullable private TextView mTextView;
    @Nullable private View mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.text_view);
        mProgressBar = findViewById(R.id.progress_bar);
        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putString("key", "Value");
                getSupportLoaderManager().initLoader(0, args, MainActivity.this);
            }
        });

        if (savedInstanceState != null) {
            mUser = savedInstanceState.getParcelable("key");
            if (mUser != null) {
                showUser();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getSupportLoaderManager().getLoader(0) != null) {
            mProgressBar.setVisibility(View.VISIBLE);
            getSupportLoaderManager().initLoader(0, null, this);
        }
    }

    @Override
    public Loader<User> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader");
        mProgressBar.setVisibility(View.VISIBLE);
        return new MyLoader(this, args);
    }

    private void showUser() {
        mTextView.setText(mUser != null ? mUser.toString() : "empty");
    }

    @Override
    public void onLoadFinished(Loader<User> loader, User data) {
        Log.d(TAG, "onLoadFinished");
        mUser = data;
        showUser();
        mProgressBar.setVisibility(View.GONE);
        getSupportLoaderManager().destroyLoader(0);
    }

    @Override
    public void onLoaderReset(Loader<User> loader) {
        mProgressBar.setVisibility(View.VISIBLE);
        Log.d(TAG, "onLoaderReset");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("key", mUser);
    }
}
